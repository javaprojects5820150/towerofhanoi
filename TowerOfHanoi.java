import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


        

public class TowerOfHanoi extends Application {
    
    
        
  
    public static void main(String[] args)throws InterruptedException {
        
        launch(args);
    }
    
    static int diskCount=5;
    private static Color BACKGROUND_COLOR = Color.rgb(13,14,13);
    private static Color BORDER_COLOR = Color.rgb(150,14,141);
    private static Color DISK_COLOR = Color.rgb(102,214,242);
    private static Color MOVE_DISK_COLOR = Color.rgb(247,244,198);

    private Canvas canvas;     
    private GraphicsContext g; 
    private int status;   
    private static final int GO = 0;          
    private static final int PAUSE = 1;   
    private static final int STEP = 2;    
    private static final int RESTART = 3;  
   
      
    private int[][] tower;
    private int[] towerHeight;
    private int moveDisk;
    private int moveTower;

    


    private Button runPauseButton;  
    private Button nextStepButton;
    private Button startOverButton;

  
   public HBox inputDisk(){
    TextField inputField = new TextField();
       
        Button add = new Button("Enter disk Count");
        HBox box=new HBox(inputField,add);
        
        
        box.setStyle("-fx-border-color: Color.WHITE; -fx-border-width: 10px 0 0 0");
        add.setOnAction(e -> {
            try {
                
                    diskCount = Integer.parseInt(inputField.getText());
                    setUpProblem();
                       
                    inputField.clear();
                } catch (NumberFormatException ex) {
                
            }
        });
        return box;
   }   

    
    public void start (Stage stage) {
        stage.setTitle("Tower Of Hanoi");
     
        canvas = new Canvas(600,503);
        g = canvas.getGraphicsContext2D();
        
        HBox box=inputDisk();
                 

        runPauseButton = new Button("Run");
        runPauseButton.setOnAction( e -> doStopGo() );
        runPauseButton.setMaxWidth(10000);
        runPauseButton.setPrefWidth(10);
        nextStepButton = new Button("Next Step");
        nextStepButton.setOnAction( e -> doNextStep() );
        nextStepButton.setMaxWidth(10000);
        nextStepButton.setPrefWidth(10);
        startOverButton = new Button("Start Over");
        startOverButton.setOnAction( e -> doRestart() );
        startOverButton.setMaxWidth(10000);
        startOverButton.setPrefWidth(10);
        startOverButton.setDisable(true);
        HBox bottom = new HBox( runPauseButton, nextStepButton, startOverButton,box);
        bottom.setStyle("-fx-border-color: Color.WHITE; -fx-border-width: 10px 0 0 0");
        HBox.setHgrow(runPauseButton, Priority.ALWAYS);
        HBox.setHgrow(nextStepButton, Priority.ALWAYS);
        HBox.setHgrow(startOverButton, Priority.ALWAYS);
        
             

        BorderPane root = new BorderPane(canvas);
        root.setBottom(bottom);
        root.setStyle("-fx-border-color: Color.BLACK); -fx-border-width: 10px");
        root.setTop(box);
        root.setStyle("-fx-border-color: Color.BLACK); -fx-border-width: 10px");
        Scene scene = new Scene(root,610,600);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

               
         
        new AnimationThread().start();                                 
                                        
    } 
   
    synchronized private void doStopGo() {
        if (status == GO) {  
            status = PAUSE;
            nextStepButton.setDisable(false);
            runPauseButton.setText("Run");
        }
        else {  
            status = GO;
            nextStepButton.setDisable(true);  
            runPauseButton.setText("Pause");
        }
        notify();  
    }
    
    synchronized private void doNextStep() {
        status = STEP;
        notify();
    }

    synchronized private void doRestart() {
        status = RESTART;
        notify();
    }

    private class AnimationThread extends Thread {
        AnimationThread() {
                
            setDaemon(true);
        }
        public void run() {
        
            while (true) {
                Platform.runLater( () -> {
                    runPauseButton.setText("Run");
                    runPauseButton.setDisable(false);
                    nextStepButton.setDisable(false);
                    startOverButton.setDisable(true);
                });
                setUpProblem();  
                status = PAUSE;
                checkStatus(); 
                Platform.runLater( () -> startOverButton.setDisable(false) );
                try {
                    solve(diskCount,0,1,2);  
                    status = PAUSE;
                    Platform.runLater( () -> { 
                        runPauseButton.setDisable(true);
                        nextStepButton.setDisable(true);
                        startOverButton.setDisable(false);
                    } );
                    checkStatus();  
                }
                catch (IllegalStateException e) {
                    
                }
            }
        }
    }

    synchronized private void checkStatus() {
        while (status == PAUSE) {
            try {
                wait();
            }
            catch (InterruptedException e) {
            }
        }
        if (status == RESTART)
            throw new IllegalStateException("Restart");
    }

    synchronized private void setUpProblem() {
        moveDisk= 0;
        tower = new int[3][diskCount];
        for (int i = 0; i < diskCount; i++)
            tower[0][i] = diskCount - i;
        

        towerHeight = new int[3];
        
            towerHeight[0] = diskCount;
        Platform.runLater( () -> drawInitialFrame() );
    }

    private void solve(int disks, int from, int to, int via) {
        if (disks == 1)
            moveOne(from,to);
        else {
            solve(disks-1, from, via, to);
            moveOne(from,to);
            solve(disks-1, via, to, from);
        }
    }


    
    synchronized private void moveOne(int fromStack, int toStack) {
        moveDisk = tower[fromStack][towerHeight[fromStack]-1];
        moveTower = fromStack;
        delay(120);
        towerHeight[fromStack]--;
        putDisk(MOVE_DISK_COLOR,moveDisk,moveTower,towerHeight[fromStack]);
        delay(80);
        putDisk(BACKGROUND_COLOR,moveDisk,moveTower,towerHeight[fromStack]);
        delay(80);
        moveTower = toStack;
        putDisk(MOVE_DISK_COLOR,moveDisk,moveTower,towerHeight[toStack]);
        delay(80);
        putDisk(DISK_COLOR,moveDisk,moveTower,towerHeight[toStack]);
        tower[toStack][towerHeight[toStack]] = moveDisk;
        towerHeight[toStack]++;
        moveDisk = 0;
        if (status == STEP)
            status = PAUSE;
        checkStatus();
    }

    synchronized private void delay(int milliseconds) {
        try {
            wait(200);
        }
        catch (InterruptedException e) {
        }
    }


   
    private void putDisk(Color color, int disk, int t, int h) {
        Platform.runLater( () -> {
            g.setFill(color);
            if (color == BACKGROUND_COLOR) {
                   
                g.fillRoundRect(75+218*t - 5*disk - 6, 373-12*h - 1, 10*disk+12, 12, 10, 10);
            }
            else {
                g.fillRoundRect(75+218*t - 5*disk - 5, 373-12*h, 10*disk+10, 10, 10, 10);
            }
        });
    }

    private void drawInitialFrame() {
        g.setFill(BACKGROUND_COLOR);
        g.fillRect(0,0,900,1000);
        g.setFill(BORDER_COLOR);
        g.fillRect(20,390,160,40);
        g.fillRect(220,390,160,40);
        g.fillRect(430,390,160,40);
        g.setFill(DISK_COLOR);
        for (int t = 0; t < 3; t++) {
            for (int i = 0; i < towerHeight[t]; i++) {
                int disk = tower[t][i];
                g.fillRoundRect(75+250*t - 5*disk - 5, 373-12*i, 10*disk+10, 10, 10, 10);
            }
        }
        g.setFill(Color.BLACK);
        g.setFont(Font.font("Arial",20));
        g.setFill(Color.BLACK);
        g.fillText("Core2web",50,413);
        g.fillText("Biencaps",260,413);
        g.fillText("Incubator",460,413);
        
    }
}